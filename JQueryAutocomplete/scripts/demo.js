﻿/*jslint  browser: true, white: true, plusplus: true */
/*global $, countries */

$(
function () {
    'use strict';


    var options, a;
    jQuery(function () {
        options = {
            serviceUrl: '../AjaxAutocomplete.ashx',
            onSelect: function (suggestion) {
                $('#selction-ajax').html('Вы выбрали: ' + suggestion.value );
            },
            onHint: function (hint) {
                $('#autocomplete-ajax-x').val(hint);
            },
            onInvalidateSelection: function () {
                $('#selction-ajax').html('You selected: none');
            }
        };
        a = $('#autocomplete-ajax').autocomplete(options);
        }); 


});