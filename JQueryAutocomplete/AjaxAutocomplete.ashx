<%@ WebHandler Language="C#" Class="Autocomplete" %>
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Script.Serialization;
using System.Linq;

public class Autocomplete : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "application/javascript";
        
        string cnString = ConfigurationManager.ConnectionStrings["MainDB"].ConnectionString;
        string sql = "SELECT Description as NAME FROM [mailstream].[dbo].[Field] where Description like '%' + @term + '%'";

        SqlCommand cmd = new SqlCommand(sql, new SqlConnection(cnString));
        cmd.CommandType= CommandType.Text;
        cmd.Parameters.AddWithValue("@term", context.Request.QueryString["query"]);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        string[] items = new string[dt.Rows.Count];
        int ctr = 0;
        
        foreach (DataRow row in dt.Rows)
        {
            items[ctr] = (string) row["NAME"] ;
            ctr++;
        }
	    var res = new Response()
	    {
		    suggestions = items
	    };
        context.Response.Write(new JavaScriptSerializer().Serialize(res));
     }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
    public class Response
    {
        public string[] suggestions { get; set; }
    }
}